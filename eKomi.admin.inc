<?php

/**
 * @file
 * Admin page callbacks for the ekomi module.
 */

/**
 * Menu callback; Displays all setting for ekomi-API.
 *
 * @ingroup forms
 * @see ekomi_admin_overview_submit()
 */
function ekomi_admin_overview() {

  // Overview of all formats.
  $error = FALSE;
  $form['api'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('eKomi Interface'),
    '#description'   => t('Informations can be found on <a href="@url" target="_blank">Customercenter</a> after login.', array('@url' => url('https://www.ekomi.de/kundenbereich.php'))),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $form['api']['ekomi_api_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('eKomi Interface ID'),
    '#default_value' => variable_get('ekomi_api_id', ''),
    '#size'          => 60,
    '#maxlength'     => 128,
    '#required'      => TRUE,
  );
  $form['api']['ekomi_api_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('eKomi Interface PW'),
    '#default_value' => variable_get('ekomi_api_key', ''),
    '#size'          => 60,
    '#maxlength'     => 128,
    '#required'      => TRUE,
  );
  $form['api']['ekomi_version'] = array(
    '#type'          => 'textfield',
    '#title'         => t('eKomi Custom Version'),
    '#default_value' => variable_get('ekomi_version', '1.0.0'),
    '#size'          => 5,
    '#maxlength'     => 128,
    '#required'      => TRUE,
  );
  $form['widgets'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Widgets'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $form['widgets']['ekomi_feedback_widget'] = array(
    '#type'          => 'textfield',
    '#title'         => t('eKomi Feedback Widget'),
    '#default_value' => variable_get('ekomi_feedback_widget', ''),
    '#size'          => 60,
    '#maxlength'     => 128,
    '#description'   => t('Enter the file name of the feedback widget. Example: vf-0000-8b6a134959b9f775503188a69478b3ca.js'),
    '#required'      => TRUE,
  );
  $form['widgets']['ekomi_widget'] = array(
    '#type'          => 'textfield',
    '#title'         => t('eKomi Block Widget'),
    '#default_value' => variable_get('ekomi_widget', ''),
    '#size'          => 60,
    '#maxlength'     => 128,
    '#description'   => t('Enter the file name of the customer opinion widget. Example: XH6PGRMMBJMPM9D.js'),
    '#required'      => TRUE,
  );
  $form['widgets']['ekomi_show_widgets'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Show widgets'),
    '#default_value' => variable_get('ekomi_show_widgets', array(NULL)),
    '#options'       => array(
      'feedback' => t('Feedback form'),
      'opinion'  => t('Customer Opinion'),
      'seal'     => t('eKomi Seal'),
    ),
    '#description'   => t(''),
  );

  //TODO: Button for the immediate transmission of the products in eKomi.

  //TODO: Checkbox for deactivate productsubmit at cronjob.
  $form['ekomi_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Submit Products at Cron.'),
    '#default_value' => variable_get('ekomi_cron', ''),
  );

  $form['buttons'] = array();
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['buttons']['send_products'] = array(
    '#type' => 'submit',
    '#value' => t('Send ALL products!'),
    '#submit' => array('ekomi_putProduct'),
  );
  $form['buttons']['send_mails'] = array(
    '#type' => 'submit',
    '#value' => t('Send ALL unsend mails!'),
    '#submit' => array('ekomi_sendMails'),
  );
  return $form;
}

function ekomi_admin_overview_submit($form, &$form_state) {
  // Process form submission to set the default format.
  if (!empty($form_state['values']['ekomi_api_id'])) {
    // Exclude unnecessary elements. This one comes from our form:
    unset($form_state['values']['submit']);

    // But these ones are put in by Forms API and always there
    unset($form_state['values']['form_id'],
      $form_state['values']['form_build_id'],
      $form_state['values']['op'],
      $form_state['values']['form_token']);

    foreach ($form_state['values'] as $key => $element) {
      variable_set($key, $element); // Serialized as necessary, so it will store an array
      }
    drupal_set_message(t('Form updated.'));
  }
}
